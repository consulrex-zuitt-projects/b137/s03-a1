package b13.consul.s03a1;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args){
        System.out.println("Factorial\n");

        Scanner appScanner = new Scanner(System.in);

        // Activity:
         /* Create a Java Program that accepts an integer and computes for
          the factorial value and displays it on the console. */
        System.out.println("Input a number: \n");
        int num = appScanner.nextInt();
        int factorial = 1;
        if(num <= 0){
            factorial = 1;
        } else {
            for(int j = 1; j <= num; j++){
                factorial = factorial * j;
            }
            System.out.println("Factorial: " + factorial);
        }

    }
}
